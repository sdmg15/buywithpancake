import axios from 'axios';
import Web3 from 'web3';

var fs = require('fs');
const abiDecoder = require('abi-decoder');

const API_KEY = "APNQMIRRSD3RK1KPICPXN9N85S7SQ251U4";
const HOST = "https://api.bscscan.com";
const ROUTER = "0x10ed43c718714eb63d5aa57b78b54704e256024e";

const instance = axios.create({
    baseURL: HOST,
    timeout: 50000
});


async function getLatestBlock() {
    try {
        const latestBlockNum = await instance.get('api/?module=proxy&action=eth_blockNumber');
        return latestBlockNum.data.result;
    } catch (error) {
        console.error(error);
    }
}

async function getNewLiqPair() {
    const latestBlockNum = await getLatestBlock();
    const startBlock = Number(latestBlockNum) - 5000;
    const URL = "api?module=account&action=txlist&address="+ ROUTER+ "&startblock="+ startBlock +"&endblock="+ latestBlockNum +"&page=1&offset=10000&sort=asc&apikey="+ API_KEY;
    try {
        const txs = await instance.get(URL);
        const res = txs.data.result;
        const pancakeswapABI = fs.readFileSync('pancake-router-abi.json', 'utf-8');
        abiDecoder.addABI( JSON.parse(pancakeswapABI));
        
        for (const iterator of res) {
            const decodedData = abiDecoder.decodeMethod(iterator.input);

            if (decodedData.name == "addLiquidity") {
                const fromAddr = iterator.from; // It maybe the contract creator
                const tokenA = decodedData.params[0].value;
                const tokenB = decodedData.params[1].value;
                const tokenADetails = await getTokenDetails(tokenA);
                const tokenBDetails = await getTokenDetails(tokenB);

                if (fromAddr == tokenADetails.owner || fromAddr == tokenBDetails.owner) {
                    console.log("Token ", tokenADetails.tokenName, "(", tokenA, ") creator is : ", tokenADetails.owner);
                    console.log("Token ", tokenBDetails.tokenName, "(", tokenB, ") creator is : ", tokenBDetails.owner);
                    console.log("=".repeat(100));
                    console.log("Token ", tokenADetails.tokenName, " added liquidity: ", decodedData.params[2].value);
                    console.log("Token ", tokenBDetails.tokenName, " added liquidity: ", decodedData.params[3].value);
                    console.log("=".repeat(100));
                }
            }

            if (decodedData.name == "addLiquidityETH") {
                const fromAddr = iterator.from;
                const tokenDetails = await getTokenDetails(decodedData.params[0].value);
                if (fromAddr == tokenDetails.owner) {
                    console.log("TOKEN WITH ADDLIQUIDITY ETH FOUND");
                }
            }
        }

    } catch (error) {
        console.error(error);
    }
}

async function getTokenDetails(addr: string) {
    let tokenName;
    try{
        const URL = "api?module=contract&action=getabi&address=" + addr + "&apikey="+API_KEY;
        const res = await instance.get(URL);
        const abi = JSON.parse(res.data.result);
        const RPC_URL =  process.env.RPC_URL || "https://bsc-dataseed.binance.org";
        const web3 = new Web3(new Web3.providers.HttpProvider(RPC_URL));
        const c = new web3.eth.Contract(abi, addr);
     
        tokenName = await c.methods.name().call();
        const owner = await c.methods.owner().call();
        return { tokenName, owner };
    }catch(e){
        console.log("This token is weird ", tokenName);
        return { tokenName };
    }
}

// getTokenDetails("0x25a1a0ff516d2500f9252fc60525e7b63e91dcd3");

getNewLiqPair();