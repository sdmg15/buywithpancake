import {
    ChainId,
    Token,
    WETH,
    Fetcher,
    Trade,
    Route,
    TokenAmount,
    TradeType,
    Percent,
    Pair,
  } from "@pancakeswap/sdk";
  
import Web3 from 'web3';
import { ethers } from "ethers";
import { JsonRpcProvider } from "@ethersproject/providers";

var fs = require('fs');

async function toggleBuy(): Promise<any> {
    const V2_SWAP_ROUTER_ADDRESS = "0x10ED43C718714eb63d5aA57B78B54704E256024E";
    const RPC_URL =  process.env.RPC_URL || "https://bsc-dataseed.binance.org";

    const web3 = new Web3(new Web3.providers.HttpProvider(RPC_URL));
    const provider = new JsonRpcProvider(RPC_URL);

    const CONTRACT_ADDRESS = web3.utils.toChecksumAddress(process.env.CONTRACT_ADDRESS as string) || "0x6B175474E89094C44Da98b954EedeAC495271d0F";
    const WALLET_PRIV_KEY = process.env.WALLET_PRIV_KEY || "null";
    const DEFAULT_PURCHASE_AMOUNT = process.env.DEFAULT_PURCHASE_AMOUNT || "0.03";
    const SLIPPAGE = process.env.SLIPPAGE || "5";

    const TokenToBuy = new Token(
        ChainId.MAINNET,
        CONTRACT_ADDRESS,
        18
    );

    const acc = web3.eth.accounts.privateKeyToAccount(WALLET_PRIV_KEY)
    const addr = acc.address;

    try{
        const de = WETH[TokenToBuy.chainId];
        const pair = await Fetcher.fetchPairData(TokenToBuy, WETH[TokenToBuy.chainId], provider);
        const route = new Route([pair as Pair], WETH[TokenToBuy.chainId]);
    
        let amount = web3.utils.toWei(DEFAULT_PURCHASE_AMOUNT as string, 'ether');
        const amountIn = amount

        console.log(DEFAULT_PURCHASE_AMOUNT + " Equals " + amount + " WEI");
        console.log("Amount of token to buy in WEI: ", amountIn);
        console.log("-".repeat(100));
        
        const trade = new Trade(
            route,
            new TokenAmount(WETH[TokenToBuy.chainId], amountIn.toString()),
            TradeType.EXACT_INPUT
        );
        const slippageTolerance = new Percent(SLIPPAGE as string, "100");

        console.log("Slippage sets to : ", slippageTolerance.toSignificant(2));
        console.log("-".repeat(100));

        const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw; 
        const path = [WETH[TokenToBuy.chainId].address, TokenToBuy.address];
        const to = addr; // should be a checksummed recipient address
        const deadline = Math.floor(Date.now() / 1000) + 60 * 20; // 20 minutes from the current Unix time 
        const value = trade.inputAmount.raw;

        var routerAbi = JSON.parse(fs.readFileSync('pancake-router-abi.json', 'utf-8'));
        var contract = new web3.eth.Contract(routerAbi, V2_SWAP_ROUTER_ADDRESS, {from: addr});

        var data = await contract.methods.swapExactETHForTokens(
            web3.utils.toHex(amountOutMin.toString()),
            path,
            to,
            deadline,
        );

        var count = await web3.eth.getTransactionCount(acc.address);
        var rawTransaction = {
            "from": acc.address,
            "gasPrice": web3.utils.toHex(5000000000),
            "gasLimit": web3.utils.toHex(290000),
            "nonce": count,
            "data": data.encodeABI(),
            "value": web3.utils.toHex(value.toString()),
            "to": V2_SWAP_ROUTER_ADDRESS,
        };

        const signedTx = await acc.signTransaction(rawTransaction);
        const res = await web3.eth.sendSignedTransaction(signedTx.rawTransaction as string);
        return res;
    }catch(e){
        const message = (e as Error).name;
        console.log("The following error occured: " + message);
    }
}

toggleBuy().then((result) => { 
    if (result){
        console.log("Transaction HASH : ", result.transactionHash);
        console.log("Gas Used : ", result.gasUsed); 
    }
});