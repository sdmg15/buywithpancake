# Setting up

```bash 

sudo apt-get update
sudo apt-get install nodejs
sudo apt-get install npm

export WALLET_PRIV_KEY=""
export SLIPPAGE="5"
export DEFAULT_PURCHASE_AMOUNT="0.03"
export CONTRACT_ADDRESS="" // The contract address of the token to buy
export RPC_URL="ws://localhost:8546"
export ROUTER_V2_ADDR="0x10ED43C718714eb63d5aA57B78B54704E256024E" // The address of the Router V2 contract pancakeswap

```